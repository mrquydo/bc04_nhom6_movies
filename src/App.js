import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import "antd/dist/antd.css";
import HomePage from "./Pages/HomePage/HomePage";
import DetailMovie from "./Pages/DetailMovie/DetailMovie";
import Layout from "./Layout/Layout";
import LoginPage from "./Pages/LoginPage/LoginPage";
import Register from "./Pages/RegisterPage/Register";
import Management from "./Pages/Management/Management";
import User from "./Pages/Management/User/User";
import UserEdit from "./Pages/Management/User/UserEdit";

function App() {

  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route path="/detail/:id" element={<Layout Component={DetailMovie} />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<Register />} />
          <Route path="/management" element={<Management Component={User} />} />
          <Route
            path="/management/user"
            element={<Management Component={User} />}
          />
          <Route
            path="/management/user/edit/:id"
            element={<Management Component={UserEdit} />}
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
