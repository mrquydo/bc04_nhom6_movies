import { userService } from "../../services/userService";
import { GET_USER_INFO_EDIT, SET_USER } from "../constants/constantUser";
import { localService } from "../../services/localService";

const setUserLoginSuccess = (successValue) => {
  return {
    type: SET_USER,
    payload: successValue,
  };
};
export const setUserLoginActionServ = (
  dataLogin,
  onLoginSuccess,
  onLoginFail
) => {
  return (dispatch) => {
    userService
      .postLogin(dataLogin)
      .then((res) => {
        localService.user.set(res.data.content);
        // console.log(res);
        onLoginSuccess(res.data.content);
        dispatch(setUserLoginSuccess(res.data.content));
      })
      .catch((err) => {
        // console.log(err);
        onLoginFail();
      });
  };
};
export const getUserInfoEdit = (data) => {
  return {
    type: GET_USER_INFO_EDIT,
    payload: data,
  };
};
