import { GET_USER_INFO_EDIT, SET_USER } from "../constants/constantUser";
import { localService } from "../../services/localService";

let initialState = {
  userInfo: localService.user.get(),
  userInfoEdit: {},
};

export const reducerUser = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER: {
      state.userInfo = action.payload;
      return { ...state };
    }
    case GET_USER_INFO_EDIT: {
      state.userInfoEdit = action.payload;
      return { ...state };
    }

    default:
      return { ...state };
  }
};
