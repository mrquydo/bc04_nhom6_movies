import { https } from "./configURL"

export const moviesServ = {

    getListMovie: () => {
        let uri = "/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05"
        return https.get(uri)
    },

    getMovieByTheater: () => {
        let uri = "/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP05"
        return https.get(uri)
    },
    getMovieBanner: () => {
        let uri = "/api/QuanLyPhim/LayDanhSachBanner"
        return https.get(uri)
    }
};