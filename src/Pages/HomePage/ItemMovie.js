import { Card } from 'antd';
import React from 'react';
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import ReactPlayer from 'react-player';
const { Meta } = Card;

export default function ItemMovie({ data }) {
    return (
        <div>
            <Card
                hoverable
                style={{
                    width: "100%",

                }}
                cover={<img className='h-80 w-full object-cover' alt="example" src={data.hinhAnh} />}
            >
                <Meta title={<p className='text-red-500 truncate'>{data.tenPhim}</p>} description={<p className='truncate'>{data.moTa}</p>} />
                <Popup modal trigger={<button className='w-full py-2 text-center bg-red-500 text-white mt-5 rounded hover:bg-black transition duration-300 cursor-pointer'>Xem Trailer</button>} position="right center">
                    <div>

                        <ReactPlayer width={750} height={500} playing={true} url={data.trailer} />
                    </div>
                </Popup>
            </Card>
        </div>
    )
}
