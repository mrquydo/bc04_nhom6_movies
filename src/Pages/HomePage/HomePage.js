import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import Spinner from "../../Components/Spinner/Spinner";
import {
  setLoadingOffAction,
  setLoadingOnAction,
} from "../../redux/actions/actionSpinner";
import { moviesServ } from "../../services/moviesService";
import Banner from "./Banner/Banner";
import ItemMovie from "./ItemMovie";
import TabsMovies from "./TabsMovies";

export default function HomePage() {
  const [movies, setMovies] = useState([]);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(setLoadingOnAction());
    moviesServ
      .getListMovie()
      .then((res) => {
        console.log(res);
        dispatch(setLoadingOffAction());
        setMovies(res.data.content);
      })
      .catch((err) => {
        dispatch(setLoadingOffAction());
        console.log(err);
      });
  }, []);
  const renderMovies = () => {
    return movies.slice(0, 10).map((data) => {
      return <ItemMovie data={data} />;
    });
  };
  return (
    <div className="container mx-auto bg-zinc-900 px-10">
      <Banner />
      <div className="grid grid-cols-5 gap-10 py-10 ">{renderMovies()}</div>
      <TabsMovies />
    </div>
  );
}
