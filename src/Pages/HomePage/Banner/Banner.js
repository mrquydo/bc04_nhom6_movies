import React, { Component } from "react";
import Slider from "react-slick";
import "./banner.css"
import pic2 from "./images/banner2.jpg"
import pic3 from "./images/banner3.jpg"
import pic6 from "./images/banner6.jpg"

export default function Banner() {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        // nextArrow: false
        // // prevArrow:,
    };
    return (
        <div>
            <h2 className="text-white text-center text-5xl font-sans"> Phim Đang Chiếu</h2>
            <Slider {...settings}>
                <div>
                    <img className="w-full h-auto object-cover" src={pic2} alt="" />
                </div>
                <div>
                    <img className="w-full h-auto" src={pic3} alt="" />
                </div>
                <div>
                    <img className="w-full object-cover" src={pic6} alt="" />
                </div>
            </Slider>
        </div>
    );
}
