import React from 'react'
import nextPic from "./images/next.png"

export default function BannerNext() {
    return (
        <div>
            <img className='w-5' src={nextPic} alt="" />
        </div>
    )
}
