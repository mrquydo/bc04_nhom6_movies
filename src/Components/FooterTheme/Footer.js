import React from 'react'
import { NavLink } from 'react-router-dom'

export default function Footer() {
    return (
        <div className='container py-10 px-10 bg-zinc-900 text-center text-white'>
            <h2 className='text-white text-4xl font-bold'>Movie Shop còn có nhiều thứ khác để xem.</h2>
            <p className='w-50 px-64 text-xl'>
                Movie Shop có một thư viện phong phú bao gồm phim truyện, phim tài liệu, chương trình truyền hình, anime, bản gốc Netflix từng đoạt giải thưởng và hơn thế nữa. Xem bao nhiêu tùy thích, bất cứ lúc nào bạn muốn.
            </p>
            <div>
                <NavLink to="/register">
                    <button className='text-white bg-red-600 p-3 border rounded border-black font-medium text-xl px-10'>
                        Đăng Ký
                    </button>
                </NavLink>
                <button className='text-white bg-red-600 p-3 border rounded border-black font-medium text-xl px-10 m-2'>
                    Liên Hệ
                </button>
            </div>

        </div>
    )
}
