

import { render } from '@testing-library/react';
import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { localService } from '../../services/localService';



export default function UserNav() {
    let user = useSelector((state) => { return state.reducerUser.userInfo; });
    console.log('user: ', user);
    let handleLogout = () => {
        localService.user.remove();
        window.location.href = "/login"
    }

    let renderContent = () => {
        if (user) {
            return (
                <>
                    <span className='font-medium text-white text-xl underline'>{user.hoTen}</span>
                    <button onClick={handleLogout} className='border rounded border-black px-5 py-2 bg-red-600 text-white'>Dang Xuat</button>
                </>
            )
        } else {
            return (
                <>
                    <NavLink to="/login">
                        <button className='border rounded border-black px-5 py-2 bg-red-600 text-white hover'>Đăng Nhập</button>
                    </NavLink>
                    <NavLink to="/register">
                        <button className='border rounded border-white bg-black px-5 py-2 text-white'>Đăng Ký</button>
                    </NavLink>
                </>
            )
        }
    }
    return (
        <div className='space-x-5'>
            {renderContent()}
        </div>
    )
}
