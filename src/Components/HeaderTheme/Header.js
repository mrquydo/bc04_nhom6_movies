import React from 'react'
import UserNav from './UserNav'

export default function Header() {
    return (
        <div className='shadow sticky top-0 z-30 w-full bg-black px-10'>
            <div className='h-20 flex justify-between items-center mx-auto container '>
                <span className='text-red-600 text-2xl font-bold animate-bounce'>The Movie Shop</span>
                <UserNav />
            </div>
        </div>
    )
}
