import React from 'react'
import Footer from '../Components/FooterTheme/Footer'
import Header from '../Components/HeaderTheme/Header'

export default function Layout({ Component }) {
    return (
        <div className>
            <Header />
            <Component />
            <Footer />
        </div>
    )
}
